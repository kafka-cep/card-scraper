package model

type Card struct {
	ID   int     `json:"id"`
	Long float64 `json:"long"`
	Lat  float64 `json:"lat"`
}
